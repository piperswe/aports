# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=plasma-browser-integration
pkgver=5.24.2
pkgrel=0
pkgdesc="Components necessary to integrate browsers into the Plasma Desktop"
# armhf blocked by extra-cmake-modules
# s390x and riscv64 blocked by polkit -> kio
# ppc64le blocked by kaccounts-integration
arch="all !armhf !s390x !riscv64 !ppc64le"
url="https://community.kde.org/Plasma/Browser_Integration"
license="GPL-3.0-or-later"
makedepends="
	extra-cmake-modules
	kactivities-dev
	kconfig-dev
	kdbusaddons-dev
	kfilemetadata-dev
	ki18n-dev
	kio-dev
	knotifications-dev
	krunner-dev
	plasma-workspace-dev
	purpose-dev
	qt5-qtbase-dev
	"

case "$pkgver" in
	*.90*) _rel=unstable;;
	*) _rel=stable;;
esac
source="https://download.kde.org/$_rel/plasma/$pkgver/plasma-browser-integration-$pkgver.tar.xz"
subpackages="$pkgname-lang"

build() {
	cmake -B build \
		-DCMAKE_BUILD_TYPE=None \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
c3444513bf2abcfa4951261c13e585857eb289f9c170f1612e9c37ac9783cb6b00f86bbdb0b207a5a180bcba3fe9ac45c4b242ed7c2decbfab7e62a25ee3a371  plasma-browser-integration-5.24.2.tar.xz
"
