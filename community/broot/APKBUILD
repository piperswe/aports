# Contributor: Leo <thinkabit.ukim@gmail.com>
# Maintainer: Jakub Jirutka <jakub@jirutka.cz>
pkgname=broot
pkgver=1.9.3
pkgrel=0
pkgdesc="New way to see and navigate directory trees"
url="https://github.com/Canop/broot"
arch="x86_64 armv7 armhf aarch64 x86 ppc64le"  # limited by rust/cargo
license="MIT"
makedepends="cargo libgit2-dev libxcb-dev oniguruma-dev"
subpackages="$pkgname-doc"
source="$pkgname-$pkgver.tar.gz::https://github.com/Canop/broot/archive/v$pkgver.tar.gz
	minimize-size.patch
	"

export RUSTONIG_DYNAMIC_LIBONIG=1  # use system libonig

prepare() {
	default_prepare

	cargo fetch --locked
}

build() {
	cargo build --release --frozen
}

check() {
	cargo test --frozen
}

package() {
	install -Dm0755 target/release/broot "$pkgdir"/usr/bin/broot
	install -Dm0644 man/page "$pkgdir"/usr/share/man/man1/broot.1
}

sha512sums="
dec999d5b00dfad7ff7104244774f124b7d4d4e7b9cedbc7c0bbda2df7be01622c65e3fa88abc6483621b697b18868bca481c82a2d4407cac1138a3f7fc5f28c  broot-1.9.3.tar.gz
05bbce2c8553beba42d13e50de87e4a320aed45787d788269388e04408cf9325dba5cd44ce24c30547483e7b6e9561d3aca17e06ca2b8097fc24a387395dac0b  minimize-size.patch
"
