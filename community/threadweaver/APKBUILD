# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=threadweaver
pkgver=5.91.0
pkgrel=0
pkgdesc="High-level multithreading framework"
arch="all !armhf" # armhf blocked by qt5-qtdeclarative
url="https://community.kde.org/Frameworks"
license="LGPL-2.1-only"
depends_dev="qt5-qtbase-dev qt5-qtdeclarative-dev"
makedepends="$depends_dev extra-cmake-modules qt5-qttools-dev doxygen"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/threadweaver-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-doc"

build() {
	cmake -B build \
		-DCMAKE_BUILD_TYPE=None \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_QCH=ON
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --build build --target install
}

sha512sums="
bb6dc30c0e4a3fd3231c81e1b222ae0351a7208e71ac87a8215156c3f2a104bfdbd03f86de934ab3bd87ce5fc08e0ded0f605870834d92181fad808c641a3af3  threadweaver-5.91.0.tar.xz
"
