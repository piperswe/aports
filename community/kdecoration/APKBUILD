# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kdecoration
pkgver=5.24.2
pkgrel=0
pkgdesc="Plugin based library to create window decorations"
arch="all !armhf"
url="https://kde.org/plasma-desktop/"
license="LGPL-2.1-only OR LGPL-3.0-only"
depends_dev="
	kcoreaddons-dev
	ki18n-dev
	qt5-qtbase-dev
	"
makedepends="$depends_dev
	extra-cmake-modules
	"
checkdepends="xvfb-run"

case "$pkgver" in
	*.90*) _rel=unstable;;
	*) _rel=stable;;
esac
source="https://download.kde.org/$_rel/plasma/$pkgver/kdecoration-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-lang"

build() {
	cmake -B build \
		-DCMAKE_BUILD_TYPE=None \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE xvfb-run ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}
sha512sums="
02ff5b561291fd74a5d81bae51bad634196d9b07bf27d1e48e7c45fafe110758d71ec5f6e2bb38922fe09e6193190d5ce447aa5f7bc41a6f81f9e7159361f899  kdecoration-5.24.2.tar.xz
"
