# Contributor: psykose <alice@ayaya.dev>
# Contributor: Alex Yam <alex@alexyam.com>
# Maintainer: psykose <alice@ayaya.dev>
pkgname=wasi-libc
pkgver=0_git20210922
_gitrev=ad5133410f66b93a2381db5b542aad5e0964db96
pkgrel=0
pkgdesc="WASI libc implementation for WebAssembly"
url="https://github.com/WebAssembly/wasi-libc"
arch="all"
options="!check" # no testsuite
# https://github.com/WebAssembly/wasi-libc/blob/main/LICENSE
#   wasi-libc			- Apache License v2.0 WITH LLVM Exceptions AND
#				  Apache License v2.0 AND MIT
#   dlmalloc/			- CC0
#   libc-bottom-half/cloudlibc/	- BSD-2-Clause
#   libc-top-half/musl/		- MIT
license="Apache-2.0 WITH LLVM-exception AND Apache-2.0 AND MIT AND CC0-1.0 AND BSD-2-Clause"
makedepends="clang llvm"
source="wasi-libc-$_gitrev.tar.gz::https://github.com/WebAssembly/wasi-libc/archive/$_gitrev.tar.gz"
builddir="$srcdir"/$pkgname-$_gitrev

build() {
	# WASM_NM & WASM_AR is auto parsed from WASM_CC
	# WASM_CFLAGS defaults to '-O2 -DNDEBUG'
	make WASM_CC=/usr/bin/clang
}

package() {
	make INSTALL_DIR="$pkgdir"/usr/share/wasi-sysroot install
}

sha512sums="
04cb3a25fef7949bf77f262bd939102f5b36e2ae85f28cdbfcd8a8984425fba54fae68049b777974bdbad96882fab383b44203e8f19a776d8a56a55475c4aab6  wasi-libc-ad5133410f66b93a2381db5b542aad5e0964db96.tar.gz
"
