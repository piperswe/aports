# Contributor: Francesco Colista <fcolista@alpinelinux.org>
# Maintainer: Francesco Colista <fcolista@alpinelinux.org>
pkgname=obs-studio
pkgver=27.2.1
pkgrel=0
pkgdesc="Free and open source software for live streaming and screen recording"
url="https://obsproject.com/"
arch="all"
license="GPL-2.0-or-later"
options="!check"
makedepends="cmake ffmpeg-dev libxinerama-dev
	qt5-qtbase-dev qt5-qtx11extras-dev qt5-qtsvg-dev x264-dev fontconfig-dev
	libxcomposite-dev freetype-dev libx11-dev mesa-dev curl-dev
	pulseaudio-dev jack-dev alsa-lib-dev fdk-aac-dev speexdsp-dev
	v4l-utils-dev jansson-dev eudev-dev swig mbedtls-dev python3-dev
	wayland-dev pipewire-dev sndio-dev libxkbcommon-dev pciutils-dev"
subpackages="$pkgname-dev"
source="https://github.com/obsproject/obs-studio/archive/$pkgver/obs-studio-$pkgver.tar.gz"

# armhf, s390x and riscv64 blocked by librsvg -> vlc
case $CARCH in
	armhf|s390x|riscv64)
		;;
	*)
		makedepends="$makedepends vlc-dev"
		;;
esac

build() {
	cmake -B build \
		-DCMAKE_BUILD_TYPE=None \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DUNIX_STRUCTURE=1 \
		-DBUILD_BROWSER=OFF \
		-DBUILD_VST=OFF \
		-DOBS_VERSION_OVERRIDE=$pkgver \
		-DENABLE_PIPEWIRE=ON
	cmake --build build
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
8992d2d8cb13e82e457a844d9ff63d57ab854a8c6f295563f86009cab801845fb093d39d315c14ef333a7a9908928ac15af4d0454ea570da4a98aff5f25f89af  obs-studio-27.2.1.tar.gz
"
