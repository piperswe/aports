# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=breeze-plymouth
pkgver=5.24.2
pkgrel=0
arch="all !armhf" # armhf blocked by extra-cmake-modules
url="https://kde.org/"
pkgdesc="Breeze theme for Plymouth"
license="GPL-2.0-or-later"
makedepends="
	extra-cmake-modules
	plymouth-dev
	"

case "$pkgver" in
	*.90*) _rel=unstable;;
	*) _rel=stable;;
esac
source="https://download.kde.org/$_rel/plasma/$pkgver/breeze-plymouth-$pkgver.tar.xz"
options="!check" # No tests

build() {
	cmake -B build \
		-DCMAKE_BUILD_TYPE=MinSizeRel \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DDISTRO_NAME="$(grep PRETTY_NAME /etc/os-release | awk -F  = '{ print $2 }')" \
		-DDISTRO_VERSION="$(grep VERSION_ID /etc/os-release | awk -F  = '{ print $2 }')"
	cmake --build build
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
ae941bb2c7e844236ad366fd171c5022bcdfba47598950001695cf79ecf70201b59ce004c65f15759ab03abe937b00023089fa1fcc2a006ac03d7533ab3d4e76  breeze-plymouth-5.24.2.tar.xz
"
