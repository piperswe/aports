# Contributor: psykose <alice@ayaya.dev>
# Maintainer: psykose <alice@ayaya.dev>
pkgname=nerdctl
pkgver=0.17.0
pkgrel=0
pkgdesc="Docker-compatible CLI for containerd"
url="https://github.com/containerd/nerdctl/"
arch="all"
license="Apache-2.0"
depends="ca-certificates containerd cni-plugins"
makedepends="go"
subpackages="$pkgname-doc"
source="$pkgname-$pkgver.tar.gz::https://github.com/containerd/nerdctl/archive/refs/tags/v$pkgver.tar.gz"
options="!check" # a lot fail

export GOFLAGS="$GOFLAGS -modcacherw -trimpath"
export GOPATH="$srcdir"

build() {
	go build -ldflags "-X github.com/containerd/nerdctl/pkg/version.Version=$pkgver" \
		-o nerdctl ./cmd/nerdctl
}

package() {
	install -Dm755 nerdctl -t "$pkgdir"/usr/bin
	install -Dm644 docs/*.md -t "$pkgdir"/usr/share/doc/$pkgname
}

sha512sums="
2edc452d7ce61abaabb73946db427347d1e8c72dc02cfd9f0375ff55f29a2770a5199b207267238c60e6bc93628171275042fbdfe1f46f67454411aad79bda1f  nerdctl-0.17.0.tar.gz
"
