# Contributor: Oleg Titov <oleg.titov@gmail.com>
# Maintainer: Oleg Titov <oleg.titov@gmail.com>
pkgname=lighthouse
pkgver=2.1.3
pkgrel=0
pkgdesc="Ethereum 2.0 Client"
url="https://lighthouse.sigmaprime.io/"
arch="x86_64 aarch64"  # limited by upstream
license="Apache-2.0"
makedepends="cargo clang-dev cmake openssl-dev protoc"
options="!check" # disable check as it takes too long
subpackages="$pkgname-doc"
source="$pkgname-$pkgver.tar.gz::https://github.com/sigp/lighthouse/archive/v$pkgver/lighthouse-$pkgver.tar.gz"

export OPENSSL_NO_VENDOR=true
export RUSTFLAGS="$RUSTFLAGS -L /usr/lib/"

build() {
	cargo build --release --locked
}

check() {
	cargo test --release --locked \
		--workspace \
			--exclude ef_tests \
			--exclude eth1 \
			--exclude genesis
}

package() {
	install -D -m755 "target/release/lighthouse" "$pkgdir/usr/bin/lighthouse"

	install -Dm 644 -t "$pkgdir/usr/share/doc/lighthouse" README.md
}

sha512sums="
892d4de52b148861d5fdadc32fbb50ed92be9bd8a08ab4ddc7c061be41f4643e5048847764ddd8c8321080c3ca5984d9e170220c6fadb85db36c79c7eb650d0a  lighthouse-2.1.3.tar.gz
"
