# Contributor: Aiden Grossman <agrossman154@yahoo.com>
# Maintainer: Aiden Grossman <agrossman154@yahoo.com>
pkgname=jupyter-nbconvert
pkgver=6.4.2
pkgrel=0
pkgdesc="Notebook conversion tool for jupyter"
url="https://github.com/jupyter/nbconvert"
arch="noarch"
license="BSD-3-Clause"
depends="py3-entrypoints
	py3-traitlets
	jupyter-nbformat
	py3-jinja2
	py3-defusedxml
	py3-mistune
	py3-pygments
	py3-pandocfilters
	py3-jupyterlab_pygments
	jupyter-nbclient
	py3-bleach
	py3-jupyter_core
	py3-testpath
	"
checkdepends="py3-pytest py3-ipykernel"
makedepends="py3-setuptools"
source="$pkgname-$pkgver.tar.gz::https://github.com/jupyter/nbconvert/archive/refs/tags/$pkgver.tar.gz"
builddir="$srcdir/nbconvert-$pkgver"

build() {
	python3 setup.py build
}

check() {
	# TestWebPDFExporter and TestNbConvertApp both require chromium to run tests
	pytest \
		-k 'not TestWebPDFExporter and not TestNbConvertApp' \
		--deselect nbconvert/exporters/tests/test_export.py::TestExport::test_export_disabled \
		--deselect nbconvert/exporters/tests/test_export.py::TestExport::test_export_filename \
		--deselect nbconvert/exporters/tests/test_export.py::TestExport::test_export_filestream \
		--deselect nbconvert/exporters/tests/test_export.py::TestExport::test_export_nbnode \
		--deselect nbconvert/exporters/tests/test_exporter.py::TestExporter::test_get_export_names_disable \
		--deselect nbconvert/exporters/tests/test_exporter.py::TestExporter::test_get_exporter_disable_config_exporters \
		--deselect nbconvert/exporters/tests/test_script.py::TestScriptExporter::test_export \
		--deselect nbconvert/exporters/tests/test_script.py::TestScriptExporter::test_export_config_transfer \
		--deselect nbconvert/exporters/tests/test_script.py::TestScriptExporter::test_export_python
}

package() {
	python3 setup.py install --prefix=/usr --root="$pkgdir"
}

sha512sums="
0d9e1d4214fc93b416fec12066b13131452c3412bdc6511b707527cabece6b8bbaa75ccb9ac269125a87940f60e52d4c475c3db4dbc41c25a0f448015d1f0970  jupyter-nbconvert-6.4.2.tar.gz
"
